#!/bin/bash
set -a
CONFIGS_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"/config
set +a
echo -ne "
-------------------------------------------------------------------------

                ░█████╗░██████╗░░█████╗░██╗░░██╗██╗
                ██╔══██╗██╔══██╗██╔══██╗██║░░██║██║
                ███████║██████╔╝██║░░╚═╝███████║██║
                ██╔══██║██╔══██╗██║░░██╗██╔══██║██║
                ██║░░██║██║░░██║╚█████╔╝██║░░██║██║
                ╚═╝░░╚═╝╚═╝░░╚═╝░╚════╝░╚═╝░░╚═╝╚═╝
                                                                                
-------------------------------------------------------------------------
                    Automated Arch Linux Installer by Josafat
-------------------------------------------------------------------------
"

( bash $CONFIGS_DIR/startup.sh )|& tee startup.log

( bash $CONFIGS_DIR/0preinstall.sh )|& tee 0-preinstall.log

( arch-chroot /mnt /mnt/root/my-arch-install/1install.sh )|& tee 1-install.log

( arch-chroot /mnt /mnt/root/my-arch-install/2postinstall.sh )|& tee 2-postinstall.log