#!/bin/bash

arch_check() {
    if [[ ! -e /etc/arch-release ]]; then
        echo -ne "ERROR! This script must be run in Arch Linux!\n"
        exit 0
    fi
}

pacman_check() {
    if [[ -f /var/lib/pacman/db.lck ]]; then
        echo "ERROR! Pacman is blocked."
        echo -ne "If not running remove /var/lib/pacman/db.lck.\n"
        exit 0
    fi
}

root_check(){
    if [[ "$(id -u)" != "0" ]]; then
        echo -ne "ERROR! This script must be run under the 'root' user!\n"
        exit 0
    fi
}

64platform_check(){
    if [[ "$(cat /sys/firmware/efi/fw_platform_size)" != "64" ]]; then
        echo -ne "ERROR! This script only works on 64bits platform!\n";
        exit 0
    fi
}

internet_check(){
    ping -q -c1 8.8.8.8 &>/dev/null && echo -ne "You are online!\n" || echo -ne 'Error! not online...\n'; exit 0
}

background_checks() {
    root_check
    arch_check
    pacman_check
    64platform_check
    internet_check
}

hwclock --systohc

background_checks
