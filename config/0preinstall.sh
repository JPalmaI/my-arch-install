#!/bin/bash
loadkeys us

timedatectl --no-ask-password  set-timezone 'America/Santiago'
timedatectl --no-ask-password  set-ntp true
pacman -S --noconfirm archlinux-keyring #update keyrings to latest to prevent packages failing to install
pacman -S --noconfirm --needed pacman-contrib grub gptfdisk
sed -i 's/^#ParallelDownloads/ParallelDownloads/' /etc/pacman.conf #enable parallel downloads

umount -A --recursive /mnt
DISK=/dev/sda
sgdisk -Z ${DISK} # zap all on disk
sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment

#create partitions
sgdisk -n 1::+3G --typecode=1:ef00 --change-name=1:'EFI' ${DISK} # partition 1 (UEFI Boot Partition)
#sgdisk -n 2::-0 --typecode=2:8300 --change-name=2:'root' ${DISK} # partition 2 (Root), default start, remaining
sgdisk -n 2::-0 --typecode=2:8e00 --change-name=2:'LVM' ${DISK} # partition 2 (Root LVM), default start, remaining

partprobe ${DISK} # reread partition table to ensure it is correct

create_physical_vol(){
    echo -ne "Creating physical volume\n"
    pvcreate ${partition2} 
    echo -ne "Done...\n"
}

create_volume_group(){
    echo -ne "Creating group volume\n"
    vgcreate archLVM ${partition2}
    echo -ne "Done...\n"
}

create_logical_volume(){
    echo -ne "Creating All 3 logical Volume...\n"
    lvcreate -n root -L 100G archLVM
    lvcreate -n swap -L 10G archLVM
    lvcreate -n var -L 20G archLVM
    lvcreate -n home -l +100%FREE archLVM
    echo -ne "Done...\n"
}

format_lvm(){
    echo -ne "Formating All logical Volumes...\n"
    mkfs.ext4 ${lvmroot}
    mkfs.ext4 ${lvmhome}
    mkfs.ext4 ${lvmvar}
    mkfs.fat -F 32 -n "EFIBOOT" ${partition1}
    echo -ne "Creating swap...\n"
    mkswap ${lvmswap}
    echo -ne "All Done...\n"

}

montar_discos(){
    echo -ne "Mounting All logical Volumes/Disks...\n"
    mount ${lvmroot} /mnt
    mount --mkdir ${lvmhome} /mnt/home
    mount --mkdir ${lvmvar} /mnt/var
    mount --mkdir -t vfat ${partition1} /mnt/boot/
    swapon ${lvmswap}
    echo -ne "Done...\n"

}

main_preinstall(){
    create_physical_vol
    create_volume_group
    create_logical_volume
    format_lvm
    montar_discos
}

if [[ "${DISK}" =~ "nvme" ]]; then
    partition1=${DISK}p1
    partition2=${DISK}p2
else
    partition1=${DISK}1
    partition2=${DISK}2
    lvm="/dev/archLVM/"
    lvmroot=${lvm}root
    lvmhome=${lvm}home
    lvmvar=${lvm}var
    lvmswap=${lvm}swap
fi

main_preinstall

pacstrap /mnt base base-devel vim sudo archlinux-keyring wget libnewt linux linux-firmware lvm2 networkmanager wireless_tools base-devel git bluez bluez-utils xdg-utils xdg-user-dirs man-db man-pages texinfo vim grub efibootmgr linux-headers --noconfirm --needed
genfstab -U /mnt >> /mnt/etc/fstab
echo " 
  Generated /etc/fstab:
"
cat /mnt/etc/fstab
cp -R ${CONFIGS_DIR} /mnt/root/my-arch-install